data "azurerm_resource_group" "main" {
  name = "${var.sharedservices_rg}"
}

resource "azurerm_shared_image_gallery" "imagegallery" {
  name                = "${var.imagegallery_name}"
  resource_group_name = "${data.azurerm_resource_group.main.name}"
  location            = "${data.azurerm_resource_group.main.location}"
  description         = "Gallery to keep the latest golden images"
}

resource "azurerm_shared_image" "windows" {
  name                = "AZ-WIN-16"
  gallery_name        = "${azurerm_shared_image_gallery.imagegallery.name}"
  resource_group_name = "${data.azurerm_resource_group.main.name}"
  location            = "${data.azurerm_resource_group.main.location}"
  os_type             = "Windows"

  identifier {
    publisher = "CET"
    offer     = "Windows"
    sku       = "2016"
  }
}

resource "azurerm_shared_image" "linux" {
  name                = "AZ-RHEL"
  gallery_name        = "${azurerm_shared_image_gallery.imagegallery.name}"
  resource_group_name = "${data.azurerm_resource_group.main.name}"
  location            = "${data.azurerm_resource_group.main.location}"
  os_type             = "Linux"

  identifier {
    publisher = "CET"
    offer     = "Linux"
    sku       = "RHEL"
  }
}
