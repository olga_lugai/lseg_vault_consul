variable "imagegallery_name" {
  default = "SharedImageGallery"
}

variable "sharedservices_rg" {
  default = "AZU-BSL-SSVS-NP-RG"
}
