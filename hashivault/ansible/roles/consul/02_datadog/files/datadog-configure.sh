#!/bin/bash

CONFIG_FILE=/etc/datadog-agent/datadog.yaml
FAILED=False

DD_API_KEY=$(curl -qs http://169.254.169.254/latest/user-data | grep '#DD_API_KEY' | awk '{ print $2 }')
PROXY_URL=$(curl -qs http://169.254.169.254/latest/user-data | grep '#PROXY_URL' | awk '{ print $2 }')


if [ -f $CONFIG_FILE ]; then 
	if [ ! -z ${DD_API_KEY} ]; then
		sed -ibk "s/REPLACE_API_KEY/${DD_API_KEY}/" $CONFIG_FILE
	else
		logger "WARNING: DD_API_KEY variable is not set in userdata!!"
		FAILED=True
	fi

	if [ ! -z ${PROXY_URL} ]; then
		sed -ibk "s@REPLACE_PROXY@${PROXY_URL}@g" $CONFIG_FILE
	else
		sed -ibk "s@REPLACE_PROXY@proxy:3128@g" $CONFIG_FILE
	fi
else
	logger "WARNING: datadog config file not found!!"
	FAILED=True
fi

if [ $FAILED = True ]; then
	echo "Agent configuration file failed check log file"
else
	service datadog-agent restart
	# Disable config script so it doesn't run again
	rm -f /etc/rc3.d/S99datadog-configure
fi