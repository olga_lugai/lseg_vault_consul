provider "azurerm" {}

module "scale-set" {
  source            = "../../../modules/vm_scale_set"
  cluster_name      = "consul_cluster"
  vault_consul_rg   = "uksouth-nonprod-vault-consul"

  #image_id            = "${module.get-ami.outputs.image_id}"
  #template_file       = "${file("${path.cwd}/user-data.tpl")}"
  storage_account_name = "stornonprodvaultconsul"
  sharedservices_vnet   = "AZU-TECH-BSL-NP-VNET"
  sharedservices_subnet = "SSVS"
  backend_pool          = "${module.load_balancer.backend_pool}"
  networking_rg         = "AZU-TECH-BSL-GATEWAY-NP-RG"
  blob_url              = "${module.blob_copy.blob_url}"
  instance_type         = "Standard_DS1_v2"
  cluster_size          = "5"
  disk_size             = "100"
  keyvault_name         = "vault-AZU-BSL-SSVS-NP-kv"
  nsg_id                = "${module.nsg.nsg_id}"
  tags                  = "${local.tags}"
  domain_name_label     = "vmtestconsul"
}

module "load_balancer" {
  source                  = "../../../modules/lb"
  cluster_name            = "consul_cluster"
  networking_rg           = "AZU-TECH-BSL-GATEWAY-NP-RG"
  sharedservices_location = "uksouth"
  sharedservices_vnet     = "AZU-TECH-BSL-NP-VNET"
  sharedservices_subnet   = "SSVS"
  vault_consul_rg         = "uksouth-nonprod-vault-consul"
  application_port        = "8500"
  tags                    = "${local.lb_tags}"
}

module "blob_copy" {
  source               = "../../../modules/blob"
  vault_consul_rg    = "uksouth-nonprod-vault-consul"
  storage_account_name = "stornonprodvaultconsul"
  container_name       = "consul-data-config"
  tags                 = "${local.tags}"

  file_path = {
    user-data.tpl = "user-data.tpl"
  }
}

module "nsg" {
  source            = "../../../modules/network_security_group"
  cluster_name      = "consul_cluster"
  tags              = "${local.tags}"
  vault_consul_rg = "uksouth-nonprod-vault-consul"
}


/*
To be used when RHEL image will be available in Shared Image Gallery"
module "get-ami" {
  source                  = "../../../modules/get_image"
  shared_image_gallery_rg = "AZU-BSL-SSVS-NP-RG"
}
*/