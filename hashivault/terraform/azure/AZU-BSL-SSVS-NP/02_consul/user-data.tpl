#!/bin/bash

########## Directs error messages to userdata.log
exec 1>/var/log/userdata.log 2>&1


########## Cleanup the proxy
echo "removing proxy settings =======================================================" >> /var/log/userdata.log
rm -f /etc/profile.d/proxy_install.sh
unset http_proxy
unset https_proxy
unset HTTP_PROXY
unset HTTPS_PROXY
env >> /var/log/userdata.log

#####Disable iptable

service iptables stop
chkconfig iptables off
service iptables status >> /var/log/userdata.log

###to be checked
########## Mounting data disk
echo "adding the data disk ========================================================" >> /var/log/userdata.log
useradd consul
mkdir /consul
dmesg | grep SCSI >> /var/log/userdata.log
parted /dev/sdc --script mklabel gpt mkpart xfspart xfs 0% 100% && partprobe /dev/sdc1 
mkfs -t ext4 /dev/sdc1 >> /var/log/userdata.log
mount /dev/sdc1 /consul >> /var/log/userdata.log
df -h  >> /var/log/userdata.log
echo /dev/sdc1  /consul ext4 defaults,nofail 0 2 >> /etc/fstab
mkdir /consul/log
mkdir /consul/tmp
mkdir /consul/config
mkdir /consul/data
chown -R consul:consul /consul
echo ""


########## Installing Az-Copy to copy config files from Azure Storage
echo "Downloading Az-Copy===============================================" >> /var/log/userdata.log
wget https://aka.ms/downloadazcopy-v10-linux
tar -xf downloadazcopy-v10-linux
cp ./azcopy_linux_amd64_*/azcopy /usr/bin/
azcopy --version >> /var/log/userdata.log

########### Installing Unzip and Consul
echo "Downloading Az-Copy===============================================" >> /var/log/userdata.log
yum install -y wget unzip
export VER="1.6.2"
wget https://releases.hashicorp.com/consul/${VER}/consul_${VER}_linux_amd64.zip
unzip consul_${VER}_linux_amd64.zip
mv consul /bin/
consul -v  >> /var/log/userdata.log


########### Installing Azure CLI
rpm --import https://packages.microsoft.com/keys/microsoft.asc
echo -e "[azure-cli]\nname=Azure CLI\nbaseurl=https://packages.microsoft.com/yumrepos/azure-cli\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" >> /etc/yum.repos.d/azure-cli.repo
yum install azure-cli -y
az --version >> /var/log/userdata.log


############# Installing dependencies for running Ansible tasks for Azure Storage
echo "Installing pip packages===============================================" >> /var/log/userdata.log
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum install ./epel-release-latest-*.noarch.rpm
wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py
pip --version >> /var/log/userdata.log
pip install packaging
pip install ansible[azure]
pip install msrest
pip install msrestazure
pip install azure 
pip install azure.storage 
pip install azure.mgmt.automation

yum install keyutils -y >> /var/log/userdata.log
keyctl new_session >> /var/log/userdata.log
export AZCOPY_SPA_CLIENT_SECRET="M1Q5.K-Tk48-NZpwwmqbP=P0QS@r0HQo" >> /var/log/userdata.log
azcopy login --service-principal --application-id "b306c98a-3d0d-4388-b530-f03ab8fda24d" --tenant-id="287e9f0e-91ec-4cf0-b7a4-c63898072181" >> /var/log/userdata.log


###Copying all files to tmp folder
echo "all static files to /tmp directory========================================================" >> /var/log/userdata.log
azcopy copy 'https://stornonprodvaultconsul.blob.core.windows.net/vault-consul-configuration-files/*' '/consul/tmp' --recursive >> /var/log/userdata.log
echo "" >> /var/log/userdata.log



echo " Configuring Datadog agent ==================" >> /var/log/userdata.log
cp /consul/tmp/datadog.yaml /etc/datadog-agent/datadog.yaml
sed -i "s/@{api_key}/data_dog_api/g" /etc/datadog-agent/datadog.yaml
sed -i "s/@{proxy_addr}/proxy_address/g" /etc/datadog-agent/datadog.yaml    
chown dd-agent:dd-agent /etc/datadog-agent/datadog.yaml 
chmod 644 /etc/datadog-agent/datadog.yaml

mkdir /etc/datadog-agent/conf.d/syslog.d
cp /consul/tmp/messages_conf.yaml /etc/datadog-agent/conf.d/syslog.d/messages_conf.yaml 
chmod -R 755 /etc/datadog-agent/conf.d/syslog.d
chmod 644 /var/log/messages


###Copying consul_conf.yaml
echo "all static files to /tmp directory========================================================" >> /var/log/userdata.log
az login --service-principal --username 'b306c98a-3d0d-4388-b530-f03ab8fda24d' --password 'M1Q5.K-Tk48-NZpwwmqbP=P0QS@r0HQo' --tenant '287e9f0e-91ec-4cf0-b7a4-c63898072181' >> /var/log/userdata.log


if [[ $(az storage blob exists --container-name 'vault-consul-configuration-files'  --name 'consul_conf.yaml'  --account-name 'stornonprodvaultconsul' --auth-mode 'login') = true ]]; then
   echo "Copying dynamic datadog agent consul config file from azure storage ======================" >> /var/log/userdata.log
   azcopy copy 'https://stornonprodvaultconsul.blob.core.windows.net/vault-consul-configuration-files/consul_conf.yaml' '/etc/datadog-agent/conf.d/consul.d/consul_conf.yaml' --recursive >> /var/log/userdata.log
   chmod 755 /etc/datadog-agent/conf.d/consul.d/consul_conf.yaml 
fi


echo "starting the datadog agent===============================================================" >> /var/log/userdata.log
systemctl stop datadog-agent.service
systemctl start datadog-agent.service
ps -ef | grep datadog >> /var/log/userdata.log
echo "" >> /var/log/userdata.log

###Configuring Consul server
echo "Configuring the consul Server============================================================" >> /var/log/userdata.log 
if [[ $(az storage blob exists --container-name 'vault-consul-configuration-files' --name 'consul_server_bootstrap.json' --account-name 'stornonprodvaultconsul' --auth-mode 'login') = true ]]; then
   azcopy copy 'https://stornonprodvaultconsul.blob.core.windows.net/vault-consul-configuration-files/consul_server_bootstrap.json' '/consul/config/config.json' >> /var/log/userdata.log
   echo "" >> /var/log/userdata.log
   export CONSUL_HTTP_TOKEN=$(cat /consul/config/config.json | grep 'master' | cut -d "\"" -f4)
else
   cp /consul/tmp/consul_server_config.json  /consul/config/config.json
fi


echo "starting the consul===============================================================" >> /var/log/userdata.log
chown -R consul:consul /consul
cp /consul/tmp/consul_server_systemd.service /etc/systemd/system/consul_server_systemd.service 
chmod 755 /etc/systemd/system/consul_server_systemd.service
cd /etc/systemd/system/ && systemctl start consul_server_systemd.service >> /var/log/userdata.log
ps -ef | grep consul >> /var/log/userdata.log
echo "" >> /var/log/userdata.log


