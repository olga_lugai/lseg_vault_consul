locals {
  tags = {
    "Project"      = "CET"
    "BusinessUnit" = "BSL"
    "Owner"        = "CloudEngineering@lseg.com"
    "CostCenter"   = "CET"
    "Environment"  = "${upper("nonprod")}"
    "Consul"       = "Server"
  }
}
