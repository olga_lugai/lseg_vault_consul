provider "azurerm" {}

module "resource_group" { 
  source               = "../../../modules/resource_group"
  resource_group_name  = "uksouth-nonprod-vault-consul"
  location             = "uksouth"
  tags                 = "${local.tags}"
}


module "storage" {
  source               = "../../../modules/storage"
  storage_account_name = "stornonprodvaultconsul"
  vault_consul_rg      = "${module.resource_group.rg_name}"
  tags                 = "${local.tags}"
}

module "blob_copy" {
  source               = "../../../modules/blob"
  storage_account_name = "${module.storage.storage_name}"
  container_name       = "vault-consul-configuration-files"
  vault_consul_rg      = "${module.resource_group.rg_name}"
  tags                 = "${local.tags}"
  
  file_path = {
    consul_agent_systemd.service  = "consul_agent_systemd.service"
    consul_server_systemd.service = "consul_server_systemd.service"
    messages_conf.yaml            = "messages_conf.yaml"
    vault_conf.yaml               = "vault_conf.yaml"
    vault_systemd.service         = "vault_systemd.service"
    consul_client_config.json     = "consul_client_config.json"
    consul_server_config.json     = "consul_server_config.json"
    datadog.yaml                  = "datadog.yaml"
  }
}

