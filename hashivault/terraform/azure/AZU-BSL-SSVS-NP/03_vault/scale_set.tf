provider "azurerm" {}

/*
To be used when RHEL image will be available in Shared Image Gallery"
module "get-ami" {
  source                  = "../../../modules/get_image"
  shared_image_gallery_rg = "AZU-BSL-SSVS-NP-RG"
}
*/

module "scale-set" {
  source          = "../../../modules/vm_scale_set"
  cluster_name    = "vault_cluster"
  vault_consul_rg = "uksouth-nonprod-vault-consul"

  #image_id            = "${module.get-ami.image_id}"
  storage_account_name  = "stornonprodvaultconsul"
  sharedservices_vnet   = "AZU-TECH-BSL-NP-VNET"
  sharedservices_subnet = "SSVS"
  backend_pool          = "${module.load-balancer.backend_pool}"
  networking_rg         = "AZU-TECH-BSL-GATEWAY-NP-RG"
  sas_token             = "${module.sas_token.sas_token}"
  blob_url              = "${module.blob_copy.blob_url}"
  instance_type         = "Standard_DS1_v2"
  cluster_size          = "3"
  disk_size             = "100"
  keyvault_name         = "vault-AZU-BSL-SSVS-NP-kv"
  keyvault_key          = "${module.keyvault.keyvault_key}"
  nsg_id                = "${module.nsg.nsg_id}"
  tags                  = "${local.tags}"
  domain_name_label     = "vmtestvault"
}

module "sas_token" {
  source               = "../../../modules/sas"
  storage_account_name = "stornonprodvaultconsul"
  vault_consul_rg      = "uksouth-nonprod-vault-consul"
}

module "keyvault" {
  source          = "../../../modules/keyvault"
  keyvault_name   = "vault-AZU-BSL-SSVS-NP-kv"
  vault_consul_rg = "uksouth-nonprod-vault-consul"
  tenant_id       = "${var.tenant_id}"
  object_id       = "${var.client_id}"
  tags            = "${local.tags}"
}

module "load-balancer" {
  source                  = "../../../modules/lb"
  cluster_name            = "vault_cluster"
  networking_rg           = "AZU-TECH-BSL-GATEWAY-NP-RG"
  sharedservices_location = "uksouth"
  sharedservices_vnet     = "AZU-TECH-BSL-NP-VNET"
  sharedservices_subnet   = "SSVS"
  vault_consul_rg         = "uksouth-nonprod-vault-consul"
  application_port        = "8500"
  tags                    = "${local.lb_tags}"
}

module "nsg" {
  source          = "../../../modules/network_security_group"
  cluster_name    = "vault_cluster"
  tags            = "${local.tags}"
  vault_consul_rg = "uksouth-nonprod-vault-consul"
}

module "blob_copy" {
  source               = "../../../modules/blob"
  vault_consul_rg      = "uksouth-nonprod-vault-consul"
  storage_account_name = "stornonprodvaultconsul"
  container_name       = "vault-data-config"
  tags                 = "${local.tags}"

  file_path = {
    user-data.tpl = "user-data.tpl"
  }
}
