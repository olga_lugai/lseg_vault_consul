variable "tenant_id" {
  default = "287e9f0e-91ec-4cf0-b7a4-c63898072181"
}

variable "client_id" {
  default = "b306c98a-3d0d-4388-b530-f03ab8fda24d"
}


locals {
  tags = {
    "Name" = "consul_cluster"
    "Project"         = "CET"
    "BusinessUnit"    = "BSL"
    "Owner"           = "CloudEngineering@lseg.com"
    "CostCenter"      = "CET"
    "environment"     = "${upper("nonprod")}"
    "consul"          = "Server"
  }

  "lb_tags" = {
    "Project"         = "CET"
    "BusinessUnit"    = "BSL"
    "Owner"           = "CloudEngineering@lseg.com"
    "CostCenter"      = "CET"
    "Environment"     = "${upper("nonprod")}"
  }
}
