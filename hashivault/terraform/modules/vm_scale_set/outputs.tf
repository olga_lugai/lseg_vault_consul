output "principal_id" {
  description = "Managed identity of a virtual machine scale set"
  value       = "${lookup(azurerm_virtual_machine_scale_set.vmss.identity[0], "principal_id")}"
}

output "scale_set_name" {
  value = "${azurerm_virtual_machine_scale_set.vmss.name}"
}
