data "azurerm_resource_group" "main" {
  name = "${var.vault_consul_rg}"
}

data "azurerm_virtual_network" "main" {
  name                = "${var.sharedservices_vnet}"
  resource_group_name = "${var.networking_rg}"
}

data "azurerm_subnet" "main" {
  name                 = "${var.sharedservices_subnet}"
  resource_group_name  = "${var.networking_rg}"
  virtual_network_name = "${var.sharedservices_vnet}"
}

data "azurerm_storage_account" "main" {
  name                = "${var.storage_account_name}"
  resource_group_name = "${data.azurerm_resource_group.main.name}"
}

resource "azurerm_virtual_machine_scale_set" "vmss" {
  name                = "${var.cluster_name}"
  location            = "${data.azurerm_resource_group.main.location}"
  resource_group_name = "${data.azurerm_resource_group.main.name}"
  upgrade_policy_mode = "Manual"

  sku {
    name     = "${var.instance_type}"
    tier     = "Standard"
    capacity = "${var.cluster_size}"
  }

  identity {
    type = "SystemAssigned"
  }

  storage_profile_image_reference {
    publisher = "center-for-internet-security-inc"
    offer     = "cis-rhel-7-v2-2-0-l1"
    sku       = "cis-rhel7-l1"
    version   = "2.0.11"
  }

  plan {
    name      = "cis-rhel7-l1"
    publisher = "center-for-internet-security-inc"
    product   = "cis-rhel-7-v2-2-0-l1"
  }

  extension {
    name                 = "BashScript"
    publisher            = "Microsoft.Azure.Extensions"
    type                 = "CustomScript"
    type_handler_version = "2.0"

    settings = <<SETTINGS
    {
      "fileUris": ["${var.blob_url}"],
      "commandToExecute": "sh user-data.tpl '${var.keyvault_name}' '${var.keyvault_key}'"
    }
    SETTINGS

    protected_settings = <<PROTECTED_SETTINGS
    {
        "storageAccountName": "${var.storage_account_name}",
        "storageAccountKey": "${data.azurerm_storage_account.main.primary_access_key}"
    }
    PROTECTED_SETTINGS
  }

  #id = "${var.image_id}"

  storage_profile_os_disk {
    name              = ""
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  storage_profile_data_disk {
    lun           = 0
    caching       = "ReadWrite"
    create_option = "Empty"
    disk_size_gb  = "${var.disk_size}"
  }
  os_profile {
    computer_name_prefix = "vmlab"
    admin_username       = "testadmin"
    admin_password       = "Password1234!"

    ### custom_data          = "${data.template_file.consul-cluster.rendered}"
  }
  os_profile_linux_config {
    disable_password_authentication = false

    ssh_keys {
      path     = "/home/testadmin/.ssh/authorized_keys"
      key_data = "${file("/lseg/jenkins/tools/keys/azure-vault-dev-uksouth.pub")}"
    }
  }
  zones = "${var.zones}"
  #####admin name should be variabilised below
  network_profile {
    name    = "vmssnetworkprofile"
    primary = true

    ip_configuration {
      name                                   = "IPConfiguration"
      subnet_id                              = "${data.azurerm_subnet.main.id}"
      load_balancer_backend_address_pool_ids = ["${var.backend_pool}"]
      primary                                = true

      public_ip_address_configuration {
        name              = "tftest-public"
        domain_name_label = "${var.domain_name_label}"
        idle_timeout      = 4
      }
    }

    network_security_group_id = "${var.nsg_id}"
  }
  tags = "${var.tags}"
}

/*
data "azurerm_subscription" "current" {}

data "azurerm_role_definition" "blob_data_contributor" {
  name = "Storage Blob Data Contributor"
}


resource "azurerm_role_assignment" "storage_blob_contributor" {
  name               = "vmss_role_assignment"
  scope              = "${data.azurerm_subscription.current.id}"
  role_definition_id = "${data.azurerm_subscription.current.id}${data.azurerm_role_definition.blob_data_contributor.id}"
  principal_id       = "${azurerm_virtual_machine_scale_set.vmss.identity.0.principal_id}"
}
*/
/*
data "template_file" "consul-cluster" {
  template = "${var.template_file}"

  vars = {
    keyvault_name = "${var.keyvault_name}"
    keyvault_key  = "${var.keyvault_key}"
  }
}
*/
resource "azurerm_monitor_autoscale_setting" "example" {
  name                = "${var.cluster_name}-setting"
  resource_group_name = "${data.azurerm_resource_group.main.name}"
  location            = "${data.azurerm_resource_group.main.location}"
  target_resource_id  = "${azurerm_virtual_machine_scale_set.vmss.id}"

  profile {
    name = "VMSSProfile"

    capacity {
      default = "${var.cluster_size}"
      minimum = "${var.cluster_size}"
      maximum = "${var.cluster_size}"
    }
  }
}

#template = "${file("${path.module}/user-data.tpl")}"


/*
data "azurerm_builtin_role_definition" "blob_storage_contributor" {
  name = "Storage Blob Data Contributor"
}

data "azurerm_storage_account" "storage" {
  name                = "${var.storage_account_name}"
  resource_group_name = "${data.azurerm_resource_group.main.name}"
}
/*
resource "azurerm_role_assignment" "storage" {
  scope              = "${data.azurerm_storage_account.storage.id}"
  role_definition_id = "${data.azurerm_storage_account.storage.id}${data.azurerm_builtin_role_definition.blob_storage_contributor.id}"
  principal_id       = "${lookup(azurerm_virtual_machine_scale_set.vmss.identity[0], "principal_id")}"
}

/*
resource "azurerm_autoscale_setting" "main" {
  name                = "myAutoscaleSetting"
  resource_group_name = "${var.sharedservices_rg}"
  location            = "${var.vmss_location}"
  target_resource_id  = "${azurerm_virtual_machine_scale_set.vmss.id}"

  profile {
    name = "defaultProfile"

    capacity {
      default = 3
      minimum = 3
      maximum = 3
    }

    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = "${azurerm_virtual_machine_scale_set.example.id}"
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "GreaterThan"
        threshold          = 75
      }

      scale_action {
        direction = "Increase"
        type      = "ChangeCount"
        value     = "1"
        cooldown  = "PT1M"
      }
    }

    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = "${azurerm_virtual_machine_scale_set.example.id}"
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "LessThan"
        threshold          = 25
      }

      scale_action {
        direction = "Decrease"
        type      = "ChangeCount"
        value     = "1"
        cooldown  = "PT1M"
      }
    }
  }

  notification {
    email {
      send_to_subscription_administrator    = true
      send_to_subscription_co_administrator = true
      custom_emails                         = ["admin@contoso.com"]
    }
  }
}

*/

