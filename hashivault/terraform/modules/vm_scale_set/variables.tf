#variable "image_id" {}

variable "sharedservices_vnet" {
  description = "Virtual network name for shared services"
}

variable "networking_rg" {
  description = "Name of a resource group with the networking resources"
}

variable "vault_consul_rg" {
  description = "Resource group name for shared services"
}

variable "sharedservices_subnet" {
  description = "Name of a shared services subnet"
}

variable "storage_account_name" {
  description = "Name of storage account with vault and consul config files"
}

variable "cluster_name" {
  description = "Name for a vm scale set"
}

variable "domain_name_label" {}

variable "instance_type" {}

variable "cluster_size" {}

variable "disk_size" {}

variable "keyvault_key" {
  description = "kms_key"
  default     = "null"
}

variable "keyvault_name" {
  default = "consulvault"
}

variable "backend_pool" {
  description = "Load balancer backend pool id to associate with vm scale set"
}

variable "nsg_id" {}

variable "blob_url" {}

variable "tags" {
  description = "Tags to apply to Azure resources"
  type        = "map"
}

variable "zones" {
  type    = "list"
  default = ["1", "2", "3"]
}

/*
variable "keyvault_key" {}
variable "keyvault_endpoint" {}
variable "env_name" {}
*/

