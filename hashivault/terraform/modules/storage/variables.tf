variable "storage_account_name" {
  default = "Storage account name for vault and consul configuration files"
}

variable "vault_consul_rg" {
  default = "Name of a resource group with vault and consul resources"
}

variable "tags" {
  description = "Tags to apply to Azure resources"
  type        = "map"
}
