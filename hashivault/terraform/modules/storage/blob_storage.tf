data "azurerm_resource_group" "main" {
  name = "${var.vault_consul_rg}"
}

resource "azurerm_storage_account" "main" {
  name                     = "${var.storage_account_name}"
  resource_group_name      = "${data.azurerm_resource_group.main.name}"
  location                 = "${data.azurerm_resource_group.main.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
  tags                     = "${var.tags}"
}
