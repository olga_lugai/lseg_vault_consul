variable "tenant_id" {
  description = "Tenant ID for a Service Principal to be used to access keys in Azure Key Vault"
}

variable "object_id" {
  description = "Object ID for a Service Principal to be used to access keys in Azure Key Vault"
}

variable "keyvault_name" {
  description = "Azure Key Vault Name for vault"
}

variable "tags" {
  description = "Tags to apply to Azure resources"
  type        = "map"
}

variable "vault_consul_rg" {
  description = "Resource group name for  vault and consul"
}
