output "keyvault_key" {
  description = "Name of a key in Azure Key Vault to be used for vault auto-unsealing"
  value       = "${azurerm_key_vault_key.main.name}"
}
