data "azurerm_resource_group" "main" {
  name = "${var.vault_consul_rg}"
}

data "azurerm_client_config" "current" {}

resource "azurerm_key_vault" "main" {
  name                        = "${var.keyvault_name}"
  location                    = "${data.azurerm_resource_group.main.location}"
  resource_group_name         = "${data.azurerm_resource_group.main.name}"
  enabled_for_disk_encryption = true
  tenant_id                   = "${var.tenant_id}"

  sku_name = "standard"

  access_policy {
    tenant_id = "${data.azurerm_client_config.current.tenant_id}"
    object_id = "${data.azurerm_client_config.current.service_principal_object_id}"

    key_permissions = [
      "get",
      "list",
      "create",
    ]

    secret_permissions = [
      "get",
    ]
  }

  access_policy {
    tenant_id = "${var.tenant_id}"
    object_id = "${var.object_id}"

    key_permissions = [
      "get",
      "create",
      "list",
    ]

    secret_permissions = [
      "get",
    ]

    storage_permissions = [
      "get",
    ]
  }

  tags = "${var.tags}"
}

resource "azurerm_key_vault_key" "main" {
  name         = "seal-vault-key"
  key_vault_id = "${azurerm_key_vault.main.id}"
  key_type     = "RSA"
  key_size     = 2048

  key_opts = [
    "decrypt",
    "encrypt",
    "sign",
    "unwrapKey",
    "verify",
    "wrapKey",
  ]
}
