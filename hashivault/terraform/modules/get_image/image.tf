data "azurerm_shared_image" "linux" {
  name                = "AZ-RHEL"
  gallery_name        = "${var.shared_image_gallery_name}"
  resource_group_name = "${var.shared_image_gallery_rg}"
}
