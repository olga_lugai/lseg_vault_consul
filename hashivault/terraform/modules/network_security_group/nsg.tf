data "azurerm_resource_group" "main" {
  name = "${var.vault_consul_rg}"
}

resource "azurerm_network_security_group" "main" {
  name                = "${var.cluster_name}-nsg"
  location            = "${data.azurerm_resource_group.main.location}"
  resource_group_name = "${data.azurerm_resource_group.main.name}"
  tags                = "${merge(var.tags, map("Name", "Security_Group"))}"
}

resource "azurerm_network_security_rule" "on_prem_access" {
  name                        = "on-prem-rule"
  resource_group_name         = "${data.azurerm_resource_group.main.name}"
  direction                   = "Inbound"
  access                      = "Allow"
  priority                    = 101
  source_address_prefix       = "10.0.0.0/8"
  source_port_range           = "*"
  destination_address_prefix  = "10.138.66.0/24"
  destination_port_range      = "443"
  protocol                    = "TCP"
  network_security_group_name = "${azurerm_network_security_group.main.name}"
}
