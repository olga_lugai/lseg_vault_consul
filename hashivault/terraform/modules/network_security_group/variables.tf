variable "vault_consul_rg" {
  description = "Resource group name for the vault and consul resources"
}

variable "cluster_name" {
  description = "Cluster name for vault/consul vmss"
}

variable "tags" {
  description = "Tags to apply to Azure resources"
  type        = "map"
}
