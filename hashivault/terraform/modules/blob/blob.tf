data "azurerm_resource_group" "main" {
  name = "${var.vault_consul_rg}"
}

data "azurerm_storage_account" "main" {
  name                = "${var.storage_account_name}"
  resource_group_name = "${data.azurerm_resource_group.main.name}"
}

/*resource "azurerm_storage_account" "main" {
  name                     = "${var.storage_account_name}"
  resource_group_name      = "${data.azurerm_resource_group.main.name}"
  location                 = "${data.azurerm_resource_group.main.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
  tags                     = "${var.tags}"
}
*/
resource "azurerm_storage_container" "main" {
  name                  = "${var.container_name}"
  storage_account_name  = "${var.storage_account_name}"
  container_access_type = "private"
}

resource "azurerm_storage_blob" "main" {
  count                  = "${length(var.file_path)}"
  name                   = "${element(keys(var.file_path),count.index)}"
  storage_account_name   = "${data.azurerm_storage_account.main.name}"
  storage_container_name = "${azurerm_storage_container.main.name}"
  type                   = "Block"
  source                 = "${element(keys(var.file_path),count.index)}"
}
