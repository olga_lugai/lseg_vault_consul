variable "vault_consul_rg" {
  description = "Resource group name for vault and consul"
}

variable "storage_account_name" {
  description = "Name of storage account with vault and consul config files"
}

variable "container_name" {
  description = "Name of a container for vault and consul config files"
}

variable "file_path" {
  description = "Path to files to be copied to storage account"
  type        = "map"
}

variable "tags" {
  description = "Tags to apply to Azure resources"
  type        = "map"
}
