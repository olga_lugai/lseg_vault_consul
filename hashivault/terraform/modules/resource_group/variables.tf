variable "tags" {
  description = "Tags to apply to Azure resources"
  type        = "map"
}

variable "resource_group_name" {
  description = "Name for a resource group with vault and consul resources"
}

variable "location" {
  description = "Location of a vault and consul resource group"
}
