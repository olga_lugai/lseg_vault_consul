data "azurerm_subnet" "main" {
  name                 = "${var.sharedservices_subnet}"
  resource_group_name  = "${var.networking_rg}"
  virtual_network_name = "${var.sharedservices_vnet}"
}

resource "azurerm_lb" "vmss" {
  name                = "${var.cluster_name}-lb"
  location            = "${var.sharedservices_location}"
  resource_group_name = "${var.vault_consul_rg}"

  frontend_ip_configuration {
    name                          = "${var.cluster_name}-lb-ip"
    subnet_id                     = "${data.azurerm_subnet.main.id}"
    private_ip_address_allocation = "Dynamic"
  }

  sku  = "Standard"
  tags = "${merge(var.tags, map("Name", "${var.cluster_name}-lb"))}"
}

resource "azurerm_lb_backend_address_pool" "bpepool" {
  resource_group_name = "${var.vault_consul_rg}"
  loadbalancer_id     = "${azurerm_lb.vmss.id}"
  name                = "BackEndAddressPool"
}

resource "azurerm_lb_probe" "vmss" {
  resource_group_name = "${var.vault_consul_rg}"
  loadbalancer_id     = "${azurerm_lb.vmss.id}"
  name                = "ssh-running-probe"
  port                = "${var.application_port}"
  number_of_probes    = "10"
  interval_in_seconds = "10"
}

resource "azurerm_lb_rule" "lbnatrule" {
  resource_group_name            = "${var.vault_consul_rg}"
  loadbalancer_id                = "${azurerm_lb.vmss.id}"
  name                           = "http"
  protocol                       = "Tcp"
  frontend_port                  = "${var.application_port}"
  backend_port                   = "${var.application_port}"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.bpepool.id}"
  frontend_ip_configuration_name = "${var.cluster_name}-lb-ip"
  probe_id                       = "${azurerm_lb_probe.vmss.id}"
}
