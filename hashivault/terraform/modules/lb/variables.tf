variable "cluster_name" {
  description = "Name for a cluster with a scale set"
}

variable "sharedservices_location" {
  description = "Location of the shared services resource group"
}

variable "sharedservices_vnet" {
  description = "Name of the virtual network for shared services"
}

variable "vault_consul_rg" {
  description = "Name of the resource group for shared services"
}

variable "application_port" {
  description = "Port on which the lb probe queries the backend pool"
}

variable "sharedservices_subnet" {}

variable "tags" {
  description = "Tags to apply to Azure resources"
  type        = "map"
}

variable "networking_rg" {
  description = "Name of a resource group with the networking resources"
}
