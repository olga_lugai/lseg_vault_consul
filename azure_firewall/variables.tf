variable "firewall_pip" {
  description = "Public IP confirguration name"
}

variable "firewall_name" {
  description = "Name for the firewall"
}

variable "sharedservices_rg" {
  description = "Name of shared services resource group name"
}

variable "sharedservices_vnet" {
  description = "Name of shared services virtual network"
}
