
###Network rule collection for Azure Firewall

resource "azurerm_firewall_network_rule_collection" "firewall" {
  name                = "az-network-rules-allow"
  azure_firewall_name = "{{ firewall_name }}"
  resource_group_name = "{{ firewal_rg }}"
  priority            = 100
  action              = "Allow"

  rule {
    name = "On-prem to Azure Vnet"

    source_addresses = [
      "{{ expressroute_gateway_IP }}",
    ]

    destination_ports = [
      "*",
    ]

    destination_addresses = [
      "{{ nonprod_vnet_address }}",
    ]

    protocols = [
      "Any"
    ]
  }

   rule {
    name = "Azure Vnet to on-prem"

    source_addresses = [
      "{{ nonprod_vnet_address }}",
    ]

    destination_ports = [
      "*",
    ]

    destination_addresses = [
      "{{ expressroute_gateway_IP }}",
    ]

    protocols = [
      "Any"
    ]
  }
}


resource "azurerm_firewall_network_rule_collection" "firewall_deny" {
  name                = "az-network-rules-deny"
  azure_firewall_name = "{{ firewall_name }}"
  resource_group_name = "{{ firewal_rg }}"
  priority            = 200
  action              = "Deny"

  rule {
    name = "DenyAll"

    source_addresses = [
      "*",
    ]

    destination_ports = [
      "*",
    ]

    destination_addresses = [
      "*"
    ]

    protocols = [
      "Any"
    ]
  }
  
}

###Only for ExpressRoute Gateway: Route table with a route rule sending all traffic to Firewall

data "azurerm_resource_group" "expressroute" {
  name = "${var.expressroute_rg}"
}

data "azurerm_firewall" "expressroute" {
  name                = "${var.firewall_name}"
  resource_group_name = "${var.sharedservices_rg}"
}

data "azurerm_subnet" "expressroute" {
  name                 = "${var.subnet_name}"
  virtual_network_name = "${var.vnet_name}"
  resource_group_name  = "${var.routetable_rg}"
}

resource "azurerm_route_table" "expressroute" {
  name                          = "ExpressRouteTable"
  location                      = "${data.azurerm_resource_group.expressroute.location}"
  resource_group_name           = "${data.azurerm_resource_group.expressroute.name}"
  disable_bgp_route_propagation = false

  route {
    name                   = "AzureFirewall"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "${data.azurerm_firewall.expressroute.ip_comfiguration.0.private_ip_address}"
  }
}

resource "azurerm_subnet_route_table_association" "expressroute {
  subnet_id      = "${data.azurerm_subnet.expressroute.id}"
  route_table_id = "${azurerm_route_table.expressroute.id}"
}

  #For all subnets: route table with a rule sending traffic to Azure Firewall

data "azurerm_resource_group" "main" {
  name = "${var.routetable_rg}"
}

data "azurerm_firewall" "main" {
  name                = "${var.firewall_name}"
  resource_group_name = "${var.sharedservices_rg}"
}

data "azurerm_subnet" "main" {
  name                 = "${var.subnet_name}"
  virtual_network_name = "${var.vnet_name}"
  resource_group_name  = "${var.routetable_rg}"
}

resource "azurerm_route_table" "main" {
  name                          = "RouteTable"
  location                      = "${data.azurerm_resource_group.main.location}"
  resource_group_name           = "${data.azurerm_resource_group.main.name}"
  disable_bgp_route_propagation = false

  route {
    name                   = "AzureFirewall"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "${data.azurerm_firewall.main.ip_comfiguration.0.private_ip_address}"
  }
}

resource "azurerm_subnet_route_table_association" "main" {
  subnet_id      = "${data.azurerm_subnet.main.id}"
  route_table_id = "${azurerm_route_table.main.id}"
}

/*
#####Creating firewall
data "azurerm_resource_group" "main" {
  name = "${var.sharedservices_rg}"
}

data "azurerm_virtual_network" "main" {
  name                = "${var.sharedservices_vnet}"
  resource_group_name = "${var.sharedservices_rg}"
}

data "azurerm_subnet" "main" {
  name                 = "AzureFirewallSubnet"
  virtual_network_name = "${var.sharedservices_vnet}"
  resource_group_name  = "${var.sharedservices_rg}"
}

resource "azurerm_public_ip" "firewall" {
  name                = "${var.firewall_pip}"
  location            = "${data.azurerm_resource_group.main.location}"
  resource_group_name = "${data.azurerm_resource_group.main.name}"
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_firewall" "firewall" {
  name                = "${var.firewall_name}"
  location            = "${data.azurerm_resource_group.main.location}"
  resource_group_name = "${data.azurerm_resource_group.main.name}"

  ip_configuration {
    name                 = "firewallipconfig"
    subnet_id            = "${data.azurerm_subnet.main.id}"
    public_ip_address_id = "${azurerm_public_ip.firewall.id}"
  }
}
*/